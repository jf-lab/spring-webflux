#!/usr/bin/env bash
echo "Let package the app"
mvn -B package
mkdir -p dist
mv ./target/*.jar ./dist/app.jar
modules_from_jar=$(jdeps --print-module-deps ./dist/app.jar)
jlink --output myjre \
      --add-modules $modules_from_jar,java.xml,jdk.unsupported,java.sql,java.naming,java.desktop,java.management,java.security.jgss,java.instrument
mv myjre dist/jre
cp ./start.sh ./dist
