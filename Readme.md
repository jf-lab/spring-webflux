# Demo webflux

A simple attempt to use webflux in a simple spring boot project. 

Also a test with WebTestClient has been created. 

## Run test

```bash
mvn test
```

## Start project

```bash
mvn spring-boot:run
```

## Package project

```
mvn package
```

## Build docker container
The docker container will be build by a multistage [dockerfile](./Dockerfile).

```bash
docker build --rm -f "Dockerfile" -t demo-spring-webflux:latest .
```
   