FROM adoptopenjdk/maven-openjdk11 AS build
COPY ./pom.xml /workbench/pom.xml
WORKDIR /workbench
RUN mvn dependency:go-offline -B
COPY ./ /workbench/
RUN ./build.sh

FROM debian:9.6-slim
COPY --from=build /workbench/dist /app/
WORKDIR /app
ENTRYPOINT [ "/app/start.sh" ]