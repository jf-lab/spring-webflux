package nl.ensignprojects.demospringwebflux;

import nl.ensignprojects.demospringwebflux.model.Order;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/orders")
public class OrderController {

    @GetMapping
    public Flux<?> getOrders() {
        Order cola = new Order("Coca-cola", 5);
        Order fanta = new Order("Fanta", 3);
        Order sevenUp = new Order("7-up", 3);
        return Flux.just(cola, fanta, sevenUp);
    }
}
