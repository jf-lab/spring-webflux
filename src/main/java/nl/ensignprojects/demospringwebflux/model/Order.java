package nl.ensignprojects.demospringwebflux.model;

import lombok.Data;

@Data
public class Order {
    private final String name;
    private final Integer quantity;
    private double price;
}
