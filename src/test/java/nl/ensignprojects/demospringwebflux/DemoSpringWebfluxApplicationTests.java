package nl.ensignprojects.demospringwebflux;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

@ExtendWith(SpringExtension.class)
@WebFluxTest
@Slf4j
class DemoSpringWebfluxApplicationTests {

    @Autowired
    private WebTestClient client;

    @Test
    @DisplayName("Test Order controller")
    void testController() {
        client = WebTestClient.bindToController(new OrderController()).build();
        client.get().uri("/orders").exchange().expectStatus().isOk();
    }

}
