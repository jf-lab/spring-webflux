#!/usr/bin/env bash
export PATH="$PWD/jre/bin":$PATH
export JAVA_TOOL_OPTIONS="-XX:+UseContainerSupport"
java -jar /app/app.jar